﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Interfaces.Persistance;
using Moq;

namespace Application.Unittests.Translator
{
    public class TranslatorBase
    {
        protected Mock<IDictionaryRepository> DictionaryRepositoryMock;

        protected TranslatorBase()
        {
            DictionaryRepositoryMock = new Mock<IDictionaryRepository>();

            DictionaryRepositoryMock
                .Setup(x => x.GetLanguages())
                .Returns(Task.FromResult((IEnumerable<string>) new[] {"EN", "DE", "IT"}));

            DictionaryRepositoryMock
                .Setup(x => x.GetTranslations(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new string[0]));

            DictionaryRepositoryMock
                .Setup(x => x.GetTranslations("EN", "DE", "hello"))
                .Returns(Task.FromResult(new[] {"hallo", "moin"}));
        }

        internal Application.Translator CreateTranslator() =>
            new Application.Translator(DictionaryRepositoryMock.Object);
    }
}