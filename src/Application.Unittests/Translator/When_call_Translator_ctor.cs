﻿using System;
using Xunit;
using Shouldly;

namespace Application.Unittests.Translator
{
    public class When_call_Translator_ctor : TranslatorBase
    {
        [Fact]
        public void then_it_fails_if_dictionaryRepository_is_null()
        {
            // Arrange

            // Act
            void Action() => new Application.Translator(null);

            // Assert
            Should.Throw<ArgumentNullException>((Action) Action);
        }
    }
}
