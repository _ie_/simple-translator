﻿using Moq;
using Shouldly;
using Xunit;

namespace Application.Unittests.Translator
{
    public class When_invoke_GetSupportedLanguages_method : TranslatorBase
    {
        [Fact]
        public void then_it_returns_supported_languages_including_AUTO()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.GetSupportedLanguages().Result;

            // Assert
            result.ShouldBe(new[] {"AUTO", "EN", "DE", "IT"});
        }

        [Fact]
        public void then_it_gets_languages_from_dictionaryRepository_if_wasnt_done_before()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.GetSupportedLanguages().Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetLanguages(), Times.Once);
        }

        [Fact]
        public void then_it_doesnt_get_languages_from_dictionaryRepository_if_was_done_before()
        {
            // Arrange
            var sut = CreateTranslator();
            var result = sut.GetSupportedLanguages().Result;
            DictionaryRepositoryMock.ResetCalls();

            // Act
            result = sut.GetSupportedLanguages().Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetLanguages(), Times.Never);
        }
    }
}