﻿using Moq;
using Shouldly;
using Xunit;

namespace Application.Unittests.Translator
{
    public class When_invoke_IsLanguageSupported_method : TranslatorBase
    {
        [Fact]
        public void then_it_returns_true_for_AUTO_if_allowed()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.IsLanguageSupported("AUTO", allowAutoLanguage: true).Result;

            // Assert
            result.ShouldBe(true);
        }

        [Fact]
        public void then_it_returns_false_for_AUTO_if_not_allowed()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.IsLanguageSupported("AUTO", allowAutoLanguage: false).Result;

            // Assert
            result.ShouldBe(false);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void then_it_returns_true_for_supported_language(bool allowAutoLanguage)
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.IsLanguageSupported("DE", allowAutoLanguage).Result;

            // Assert
            result.ShouldBe(true);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void then_it_returns_false_for_non_supported_language(bool allowAutoLanguage)
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.IsLanguageSupported("AZ", allowAutoLanguage).Result;

            // Assert
            result.ShouldBe(false);
        }

        [Fact]
        public void then_it_gets_languages_from_dictionaryRepository_if_wasnt_done_before()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.IsLanguageSupported("AUTO", allowAutoLanguage: false).Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetLanguages(), Times.Once);
        }

        [Fact]
        public void then_it_doesnt_get_languages_from_dictionaryRepository_if_was_done_before()
        {
            // Arrange
            var sut = CreateTranslator();
            var result = sut.IsLanguageSupported("AUTO", allowAutoLanguage: false).Result;
            DictionaryRepositoryMock.ResetCalls();

            // Act
            result = sut.IsLanguageSupported("AUTO", allowAutoLanguage: false).Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetLanguages(), Times.Never);
        }
    }
}