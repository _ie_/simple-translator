﻿using Moq;
using Shouldly;
using Xunit;

namespace Application.Unittests.Translator
{
    public class When_invoke_Translate_method : TranslatorBase
    {
        [Fact]
        public void then_it_returns_translations_if_found()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var (sourceLanguage, translations) = sut.Translate("EN", "DE", "hello").Result;

            // Assert
            sourceLanguage.ShouldBe("EN");
            translations.ShouldBe(new[] {"hallo", "moin"});
        }

        [Fact]
        public void then_it_returns_no_translations_if_not_found()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.Translate("EN", "DE", "hello111").Result;

            // Assert
            result.translations.ShouldBeEmpty();
        }

        [Fact]
        public void then_it_requests_translations_in_dictionaryRepository()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.Translate("EN", "DE", "hello").Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetTranslations("EN", "DE", "hello"), Times.Once);
        }

        [Fact]
        public void then_it_requests_translations_in_dictionaryRepository_for_every_language_until_not_found()
        {
            // Arrange
            var sut = CreateTranslator();

            // Act
            var result = sut.Translate("AUTO", "DE", "hello111").Result;

            // Assert
            DictionaryRepositoryMock.Verify(x => x.GetTranslations("EN", "DE", "hello111"), Times.Once);
            DictionaryRepositoryMock.Verify(x => x.GetTranslations("IT", "DE", "hello111"), Times.Once);
        }
    }
}