﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.UseCases.Commands;
using Moq;
using Shouldly;
using Xunit;

namespace Application.Unittests.UseCases.Commands.TranslateCommand
{
    public class TranslateCommandBase
    {
        internal readonly Mock<ITranslator> TranslatorMock;

        protected TranslateCommandBase()
        {
            TranslatorMock = new Mock<ITranslator>();

            TranslatorMock.Setup(x => x.GetSupportedLanguages())
                .Returns(Task.FromResult((IEnumerable<string>)new[] { "AUTO", "EN", "DE", "IT" }));

            TranslatorMock.Setup(x => x.IsLanguageSupported(It.IsAny<string>(), true))
                .Returns<string, bool>((language, auto) => Task.FromResult(new[] {"AUTO", "EN", "DE", "IT"}.Contains(language)));

            TranslatorMock.Setup(x => x.IsLanguageSupported(It.IsAny<string>(), false))
                .Returns<string, bool>((language, auto) => Task.FromResult(new[] {"EN", "DE", "IT"}.Contains(language)));

            TranslatorMock.Setup(x => x.Translate("EN", "DE", "hello"))
                .Returns(Task.FromResult(("EN", new[] {"hallo", "moin"})));
        }
    }

    public class When_invoke_Execute_method : TranslateCommandBase
    {
        [Fact]
        public void then_it_returns_translation_result()
        {
            // Arrange
            var sut = new Application.UseCases.Commands.TranslateCommand(TranslatorMock.Object);
            var request = new TranslationRequest
            {
                SourceLanguage = "EN",
                TargetLanguage = "DE",
                Word = "hello"
            };

            // Act
            var result = sut.Execute(request).Result;

            // Assert
            result.ShouldNotBeNull();
        }

        [Fact]
        public void then_it_checks_whether_both_languages_supported_by_Translator()
        {
            // Arrange
            var sut = new Application.UseCases.Commands.TranslateCommand(TranslatorMock.Object);
            var request = new TranslationRequest
            {
                SourceLanguage = "EN",
                TargetLanguage = "DE",
                Word = "hello"
            };

            // Act
            var result = sut.Execute(request).Result;

            // Assert
            TranslatorMock.Verify(x => x.IsLanguageSupported("EN", It.IsAny<bool>()), Times.Once);
            TranslatorMock.Verify(x => x.IsLanguageSupported("DE", It.IsAny<bool>()), Times.Once);
        }

        [Fact]
        public void then_it_requests_translation_on_Translator()
        {
            // Arrange
            var sut = new Application.UseCases.Commands.TranslateCommand(TranslatorMock.Object);
            var request = new TranslationRequest
            {
                SourceLanguage = "EN",
                TargetLanguage = "DE",
                Word = "hello"
            };

            // Act
            var result = sut.Execute(request).Result;

            // Assert
            TranslatorMock.Verify(x => x.Translate("EN", "DE", "hello"), Times.Once);
        }
    }

}
