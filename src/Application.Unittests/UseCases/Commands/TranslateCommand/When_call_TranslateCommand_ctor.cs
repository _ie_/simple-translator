﻿using System;
using Shouldly;
using Xunit;

namespace Application.Unittests.UseCases.Commands.TranslateCommand
{
    public class When_call_TranslateCommand_ctor : TranslateCommandBase
    {
        [Fact]
        public void then_it_fails_if_translator_is_null()
        {
            // Arrange

            // Act
            void Action() => new Application.UseCases.Commands.TranslateCommand(null);

            // Assert
            Should.Throw<ArgumentNullException>((Action)Action);
        }
    }
}