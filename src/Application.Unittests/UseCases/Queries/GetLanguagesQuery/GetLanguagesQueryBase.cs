﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;

namespace Application.Unittests.UseCases.Queries.GetLanguagesQuery
{
    public class GetLanguagesQueryBase
    {
        internal readonly Mock<ITranslator> TranslatorMock;

        protected GetLanguagesQueryBase()
        {
            TranslatorMock = new Mock<ITranslator>();

            TranslatorMock.Setup(x => x.GetSupportedLanguages())
                .Returns(Task.FromResult((IEnumerable<string>) new[] {"AUTO", "EN", "DE", "IT"}));
        }
    }
}
