﻿using System;
using Shouldly;
using Xunit;

namespace Application.Unittests.UseCases.Queries.GetLanguagesQuery
{
    public class When_call_GetLanguagesQuery_ctor : GetLanguagesQueryBase
    {
        [Fact]
        public void then_it_fails_if_translator_is_null()
        {
            // Arrange

            // Act
            void Action() => new Application.UseCases.Queries.GetLanguagesQuery(null);

            // Assert
            Should.Throw<ArgumentNullException>((Action)Action);
        }
    }
}
