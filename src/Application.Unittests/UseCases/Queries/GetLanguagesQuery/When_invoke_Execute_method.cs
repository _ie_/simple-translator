﻿using Moq;
using Shouldly;
using Xunit;

namespace Application.Unittests.UseCases.Queries.GetLanguagesQuery
{
    public class When_invoke_Execute_method : GetLanguagesQueryBase
    {
        [Fact]
        public void then_it_returns_supported_languages()
        {
            // Arrange
            var sut = new Application.UseCases.Queries.GetLanguagesQuery(TranslatorMock.Object);

            // Act
            var result = sut.Execute().Result;

            // Assert
            result.Languages.ShouldBe(new[] {"AUTO", "EN", "DE", "IT"});
        }

        [Fact]
        public void then_it_requests_supported_languages_on_Translator()
        {
            // Arrange
            var sut = new Application.UseCases.Queries.GetLanguagesQuery(TranslatorMock.Object);

            // Act
            var result = sut.Execute().Result;

            // Assert
            TranslatorMock.Verify(x => x.GetSupportedLanguages(), Times.Once);
        }
    }
}