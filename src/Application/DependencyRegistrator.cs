﻿using Application.UseCases.Commands;
using Application.UseCases.Queries;
using Catel.IoC;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Application.Unittests")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Application
{
    public static class DependencyRegistrator
    {
        public static void Register(IServiceLocator serviceLocator)
        {
            serviceLocator.RegisterType<ITranslator, Translator>();
            serviceLocator.RegisterType<IGetLanguagesQuery, GetLanguagesQuery>();
            serviceLocator.RegisterType<ITranslateCommand, TranslateCommand>();
        }
    }
}
