﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application
{
    internal interface ITranslator
    {
        Task<bool> IsLanguageSupported(string language, bool allowAutoLanguage);
        Task<IEnumerable<string>> GetSupportedLanguages();
        Task<(string sourceLanguage, string[] translations)> Translate(string sourceLanguage, string targetLanguage, string word);
    }
}
