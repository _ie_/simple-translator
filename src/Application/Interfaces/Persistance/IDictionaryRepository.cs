﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.Persistance
{
    public interface IDictionaryRepository
    {
        Task<IEnumerable<string>> GetLanguages();
        Task<string[]> GetTranslations(string sourceLanguage, string targetLanguage, string word);
    }
}