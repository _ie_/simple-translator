﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces.Persistance;

namespace Application
{
    internal class Translator : ITranslator
    {
        private readonly IDictionaryRepository _dictionaryRepository;
        private bool _initializationRequired = true;
        private HashSet<string> _languages;

        public Translator(IDictionaryRepository dictionaryRepository)
        {
            _dictionaryRepository = dictionaryRepository ?? throw new ArgumentNullException(nameof(dictionaryRepository));
        }

        public async Task<bool> IsLanguageSupported(string language, bool allowAutoLanguage)
        {
            await EnsureInitialized();

            return language == "AUTO" && allowAutoLanguage || _languages.Contains(language);
        }

        public async Task<IEnumerable<string>> GetSupportedLanguages()
        {
            await EnsureInitialized();

            return new[] {"AUTO"}.Concat(_languages).ToArray();
        }

        public async Task<(string sourceLanguage, string[] translations)> Translate(string sourceLanguage, string targetLanguage, string word)
        {
            await EnsureInitialized();

            var translations = new string[0];

            if (sourceLanguage == "AUTO")
            {
                foreach (var language in _languages)
                {
                    translations = await _dictionaryRepository.GetTranslations(language, targetLanguage, word);
                    if (translations.Length > 0)
                    {
                        sourceLanguage = language;
                        break;
                    }
                }
            }
            else
            {
                translations = await _dictionaryRepository.GetTranslations(sourceLanguage, targetLanguage, word);
            }

            return (sourceLanguage, translations);
        }

        private async Task EnsureInitialized()
        {
            if (_initializationRequired)
            {
                _languages = new HashSet<string>(await _dictionaryRepository.GetLanguages());
                _initializationRequired = false;
            }
        }
    }
}