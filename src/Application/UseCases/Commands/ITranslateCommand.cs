﻿using System.Threading.Tasks;

namespace Application.UseCases.Commands
{
    public interface ITranslateCommand
    {
        Task<TranslationResponse> Execute(TranslationRequest request);
    }
}
