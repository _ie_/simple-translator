﻿using System;
using System.Threading.Tasks;

namespace Application.UseCases.Commands
{
    internal class TranslateCommand : ITranslateCommand
    {
        private readonly ITranslator _translator;

        public TranslateCommand(ITranslator translator)
        {
            _translator = translator ?? throw new ArgumentNullException(nameof(translator));
        }

        public async Task<TranslationResponse> Execute(TranslationRequest request)
        {
            if (!await _translator.IsLanguageSupported(request.SourceLanguage, true))
                throw new ArgumentException("Specified source language is not supported", nameof(request));

            if (!await _translator.IsLanguageSupported(request.TargetLanguage, false))
                throw new ArgumentException("Specified target language is not supported", nameof(request));

            var (sourceLanguage, translations) = await _translator.Translate(request.SourceLanguage, request.TargetLanguage, request.Word);

            return new TranslationResponse
            {
                SourceLanguage = sourceLanguage,
                TargetLanguage = request.TargetLanguage,
                Word = request.Word,
                Translations = translations
            };
        }
    }
}
