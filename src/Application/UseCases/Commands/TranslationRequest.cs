﻿namespace Application.UseCases.Commands
{
    public class TranslationRequest
    {
        public string SourceLanguage { get; set; }
        public string TargetLanguage { get; set; }
        public string Word { get; set; }
    }
}
