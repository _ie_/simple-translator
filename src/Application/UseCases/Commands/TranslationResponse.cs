﻿namespace Application.UseCases.Commands
{
    public class TranslationResponse
    {
        public string SourceLanguage { get; set; }
        public string TargetLanguage { get; set; }
        public string Word { get; set; }
        public string[] Translations { get; set; }
    }
}
