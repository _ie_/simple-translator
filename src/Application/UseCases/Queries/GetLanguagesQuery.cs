﻿using System;
using System.Threading.Tasks;

namespace Application.UseCases.Queries
{
    internal class GetLanguagesQuery : IGetLanguagesQuery
    {
        private readonly ITranslator _translator;

        public GetLanguagesQuery(ITranslator translator)
        {
            _translator = translator ?? throw new ArgumentNullException(nameof(translator));
        }

        public async Task<GetLanguagesResult> Execute()
        {
            var languages = await _translator.GetSupportedLanguages();
            return new GetLanguagesResult {Languages = languages};
        }
    }
}