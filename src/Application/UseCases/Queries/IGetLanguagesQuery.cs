﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.UseCases.Queries
{
    public interface IGetLanguagesQuery
    {
        Task<GetLanguagesResult> Execute();
    }

    public class GetLanguagesResult
    {
        public IEnumerable<string> Languages { get; set; }
    }
}
