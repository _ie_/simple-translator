﻿using Shouldly;
using Xunit;

namespace Persistance.Unittests.DictionaryLoader
{
    public class When_invoke_GetDictionaryDescriptors_method : DictionaryLoaderBase
    {
        [Fact]
        public void then_it_returns_list_of_available_dictionaries()
        {
            // Arrange
            var sut = new Persistance.DictionaryLoader();

            // Act
            var dictionaryDescriptors = sut.GetDictionaryDescriptors().Result;

            // Assert
            dictionaryDescriptors.ShouldNotBeEmpty();
        }
    }
}