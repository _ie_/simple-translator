﻿using System;
using System.Linq;
using Shouldly;
using Xunit;

namespace Persistance.Unittests.DictionaryLoader
{
    public class When_invoke_LoadDictionary_method : DictionaryLoaderBase
    {
        [Fact]
        public void then_it_returns_loaded_dictionary()
        {
            // Arrange
            var sut = new Persistance.DictionaryLoader();
            var dictionaryDescriptors = sut.GetDictionaryDescriptors().Result;

            // Act
            var dictionary = sut.LoadDictionary(dictionaryDescriptors.First()).Result;

            // Assert
            dictionary.ShouldNotBeEmpty();
        }

        [Fact]
        public void then_it_fails_if_dictionary_does_not_exist()
        {
            // Arrange
            var sut = new Persistance.DictionaryLoader();

            // Act
            void Action()
            {
                var dict = sut.LoadDictionary(new DictionaryDescriptor
                {
                    SourceLanguage = "XX",
                    TargetLanguage = "YY"
                }).Result;
            }

            // Assert
            Should.Throw<Exception>((Action) Action);
        }
    }
}