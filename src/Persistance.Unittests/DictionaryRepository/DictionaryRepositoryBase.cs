﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;

namespace Persistance.Unittests.DictionaryRepository
{
    public class DictionaryRepositoryBase
    {
        protected Mock<IDictionaryLoader> DictionaryLoader { get; }

        protected DictionaryRepositoryBase()
        {
            DictionaryLoader = new Mock<IDictionaryLoader>();

            DictionaryLoader.Setup(x => x.GetDictionaryDescriptors()).Returns(Task.FromResult((IEnumerable<DictionaryDescriptor>) new[]
            {
                new DictionaryDescriptor {SourceLanguage = "EN", TargetLanguage = "DE"},
                new DictionaryDescriptor {SourceLanguage = "EN", TargetLanguage = "RU"},
                new DictionaryDescriptor {SourceLanguage = "DE", TargetLanguage = "RU"},
            }));

            DictionaryLoader.Setup(x => x.LoadDictionary(It.IsAny<DictionaryDescriptor>()))
                .Returns<DictionaryDescriptor>(descriptor =>
                {
                    var dict = new Dictionary<string, string[]>();
                    if (descriptor.SourceLanguage == "EN" && descriptor.TargetLanguage == "DE")
                    {
                        dict["hello"] = new[] { "hallo", "moin" };
                    }

                    return Task.FromResult((IDictionary<string, string[]>) dict);
                });
        }

        protected Persistance.DictionaryRepository CreateDictionaryRepository() => new Persistance.DictionaryRepository(DictionaryLoader.Object);
    }
}
