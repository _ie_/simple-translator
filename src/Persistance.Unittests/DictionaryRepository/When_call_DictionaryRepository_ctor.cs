﻿using System;
using Shouldly;
using Xunit;

namespace Persistance.Unittests.DictionaryRepository
{
    public class When_call_DictionaryRepository_ctor : DictionaryRepositoryBase
    {
        [Fact]
        public void then_it_fails_if_dictionaryLoader_is_null()
        {
            // Arrange

            // Act
            void Action() => new Persistance.DictionaryRepository(null);

            // Assert
            Should.Throw<ArgumentNullException>((Action) Action);
        }
    }
}