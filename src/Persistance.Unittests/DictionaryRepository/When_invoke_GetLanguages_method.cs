﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using Shouldly;
using Xunit;

namespace Persistance.Unittests.DictionaryRepository
{
    public class When_invoke_GetLanguages_method : DictionaryRepositoryBase
    {
        [Fact]
        public void then_it_delivers_all_languages_of_existing_dictionaries()
        {
            // Arrange
            var sut = CreateDictionaryRepository();

            // Act
            var laguages = sut.GetLanguages().Result;

            // Assert
            laguages.ShouldBe(new[] { "EN", "DE", "RU" });
        }

        [Fact]
        public void then_it_delivers_nothing_if_no_dictionaries_exist()
        {
            // Arrange
            DictionaryLoader
                .Setup(x => x.GetDictionaryDescriptors())
                .Returns(Task.FromResult((IEnumerable<DictionaryDescriptor>) new DictionaryDescriptor[0]));

            var sut = CreateDictionaryRepository();

            // Act
            var laguages = sut.GetLanguages().Result;

            // Assert
            laguages.ShouldBeEmpty();
        }

        [Fact]
        public void then_IDictionaryLoader_GetDictionaries_called_once()
        {
            // Arrange
            var sut = CreateDictionaryRepository();

            // Act
            var laguages = sut.GetLanguages().Result;

            // Assert
            DictionaryLoader.Verify(x => x.GetDictionaryDescriptors(), Times.Once);
        }
    }
}