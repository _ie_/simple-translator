﻿using Moq;
using Xunit;

namespace Persistance.Unittests.DictionaryRepository
{
    public class When_invoke_GetTranslations_method : DictionaryRepositoryBase
    {
        [Fact]
        public void then_it_checks_whether_dictionary_exists_using_DictionaryLoader_GetDictionaryDescriptors_method()
        {
            // Arrange
            var sut = CreateDictionaryRepository();

            // Act
            var translations = sut.GetTranslations("EN", "DE", "hello").Result;

            // Assert
            DictionaryLoader.Verify(x => x.GetDictionaryDescriptors(), Times.Once);
        }

        [Fact]
        public void then_it_loads_dictionary_using_DictionaryLoader_LoadDictionary_method()
        {
            // Arrange
            var sut = CreateDictionaryRepository();

            // Act
            var translations = sut.GetTranslations("EN", "DE", "hello").Result;

            // Assert
            DictionaryLoader.Verify(x => x.LoadDictionary(It.IsAny<DictionaryDescriptor>()), Times.Once);
        }

        [Fact]
        public void then_it_does_not_load_dictionary_twice_if_already_loaded()
        {
            // Arrange
            var sut = CreateDictionaryRepository();

            // Act
            var translations = sut.GetTranslations("EN", "DE", "hello").Result;
            translations = sut.GetTranslations("EN", "DE", "check").Result;

            // Assert
            DictionaryLoader.Verify(x => x.LoadDictionary(It.IsAny<DictionaryDescriptor>()), Times.Once);
        }
    }
}