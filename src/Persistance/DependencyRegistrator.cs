﻿using Application.Interfaces.Persistance;
using Catel.IoC;

namespace Persistance
{
    public static class DependencyRegistrator
    {
        public static void Register(IServiceLocator serviceLocator)
        {
            serviceLocator.RegisterType<IDictionaryRepository, DictionaryRepository>();
            serviceLocator.RegisterType<IDictionaryLoader, DictionaryLoader>();
        }
    }
}
