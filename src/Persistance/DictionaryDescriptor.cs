﻿namespace Persistance
{
    public class DictionaryDescriptor
    {
        public string SourceLanguage { get; set; }
        public string TargetLanguage { get; set; }
    }
}