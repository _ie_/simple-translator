﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Persistance
{
    public class DictionaryLoader : IDictionaryLoader
    {
        public Task<IEnumerable<DictionaryDescriptor>> GetDictionaryDescriptors()
        {
            return Task.Run(() =>
            {
                var assembly = Assembly.GetExecutingAssembly();
                return (IEnumerable<DictionaryDescriptor>)assembly.GetManifestResourceNames()
                    .Select(x => Regex.Match(x, @"\._dict\.(?'source'\w{2})_(?'target'\w{2})\.xml"))
                    .Where(x => x.Success)
                    .Select(x => new DictionaryDescriptor {
                        SourceLanguage = x.Groups["source"].Value.ToUpper(),
                        TargetLanguage = x.Groups["target"].Value.ToUpper()
                    })
                    .ToArray();
            });
            
        }

        public Task<IDictionary<string, string[]>> LoadDictionary(DictionaryDescriptor dictionaryDescriptor)
        {
            return Task.Run(() =>
            {
                var assembly = Assembly.GetExecutingAssembly();
                var (source, target) = (dictionaryDescriptor.SourceLanguage, dictionaryDescriptor.TargetLanguage);
                var resourceName = assembly.GetManifestResourceNames()
                    .FirstOrDefault(x => Regex.Match(x, $@"\._dict\.{source}_{target}\.xml", RegexOptions.IgnoreCase).Success);

                if (resourceName == null)
                    throw new ArgumentException("No dictionary found for specified descriptor", nameof(dictionaryDescriptor));

                using (var stream = assembly.GetManifestResourceStream(resourceName))
                {
                    var dict = new Dictionary<string, string[]>();
                    var doc = new XmlDocument();
                    doc.Load(stream);
                    var wordNodes = doc.SelectNodes("/dictionary/words/word");
                    foreach (XmlNode word in wordNodes)
                    {
                        var text = word.Attributes["text"].Value;
                        var translationNodes = word.SelectNodes("translations/translation");
                        var translations = new List<string>();
                        foreach (XmlNode translation in translationNodes)
                        {
                            translations.Add(translation.Attributes["text"].Value);
                        }

                        dict[text] = translations.ToArray();
                    }

                    return (IDictionary<string, string[]>) dict;
                }
            });
        }
    }
}