﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces.Persistance;

namespace Persistance
{
    public class DictionaryRepository : IDictionaryRepository
    {
        private readonly IDictionaryLoader _dictionaryLoader;

        private readonly IDictionary<string, IDictionary<string, string[]>> _cachedDictionaries =
            new ConcurrentDictionary<string, IDictionary<string, string[]>>();

        public DictionaryRepository(IDictionaryLoader dictionaryLoader)
        {
            _dictionaryLoader = dictionaryLoader ?? throw new ArgumentNullException(nameof(dictionaryLoader));
        }

        public async Task<IEnumerable<string>> GetLanguages()
        {
            var dictionaries = await _dictionaryLoader.GetDictionaryDescriptors();
            return dictionaries.SelectMany(x => new[] {x.SourceLanguage, x.TargetLanguage}).Distinct().ToArray();
        }

        public Task<string[]> GetTranslations(string sourceLanguage, string targetLanguage, string word)
        {
            return Task.Run(async () =>
            {
                var key = sourceLanguage + "_" + targetLanguage;
                if (!_cachedDictionaries.ContainsKey(key))
                {
                    var dictionaries = await _dictionaryLoader.GetDictionaryDescriptors();
                    var dictionaryInfo = dictionaries.FirstOrDefault(x =>
                        x.SourceLanguage == sourceLanguage && x.TargetLanguage == targetLanguage);

                    if (dictionaryInfo == null)
                        return new string[0];

                    var dict = await _dictionaryLoader.LoadDictionary(dictionaryInfo);
                    _cachedDictionaries[key] = dict;
                }

                if (_cachedDictionaries[key].ContainsKey(word))
                    return _cachedDictionaries[key][word];

                return new string[0];
            });
        }
    }
}
