﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Persistance
{
    public interface IDictionaryLoader
    {
        Task<IEnumerable<DictionaryDescriptor>> GetDictionaryDescriptors();
        Task<IDictionary<string, string[]>> LoadDictionary(DictionaryDescriptor dictionaryDescriptor);
    }
}