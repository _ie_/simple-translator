﻿using System;
using System.Globalization;
using Presentation.Models;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.Converters.EqualityToEnumConverter
{
    public class When_invoke_Convert_method : EqualityToEnumConverterBase
    {
        [Fact]
        public void then_it_delivers_true_if_same_values_passed()
        {
            // Arrange
            var sut = new Presentation.Converters.EqualityToEnumConverterImpl();

            // Act
            var result = sut.Convert(TranslationStatus.Error, typeof(bool), TranslationStatus.Error, CultureInfo.InvariantCulture);

            // Assert
            result.ShouldBe(true);
        }

        [Fact]
        public void then_it_delivers_true_if_different_values_passed()
        {
            // Arrange
            var sut = new Presentation.Converters.EqualityToEnumConverterImpl();

            // Act
            var result = sut.Convert(TranslationStatus.Error, typeof(bool), TranslationStatus.Success, CultureInfo.InvariantCulture);

            // Assert
            result.ShouldBe(false);
        }

    }

    public class When_invoke_ConvertBack_method : EqualityToEnumConverterBase
    {
        [Fact]
        public void then_it_delivers_true_if_same_values_passed()
        {
            // Arrange
            var sut = new Presentation.Converters.EqualityToEnumConverterImpl();

            // Act
            void Action() => sut.ConvertBack(TranslationStatus.Error, typeof(bool), TranslationStatus.Error, CultureInfo.InvariantCulture);

            // Assert
            Should.Throw<NotSupportedException>((Action) Action);
        }
    }
}