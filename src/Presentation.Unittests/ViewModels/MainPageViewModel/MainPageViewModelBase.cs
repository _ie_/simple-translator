﻿using Application.UseCases.Commands;
using Application.UseCases.Queries;
using Moq;

namespace Presentation.Unittests.ViewModels.MainPageViewModel
{
    public class MainPageViewModelBase
    {
        protected readonly Presentation.ViewModels.TranslatorViewModel TranslatorViewModel;
        protected readonly Mock<IGetLanguagesQuery> GetLanguagesQueryMock;
        protected readonly Mock<ITranslateCommand> TranslateCommandMock;

        protected MainPageViewModelBase()
        {
            GetLanguagesQueryMock = new Mock<IGetLanguagesQuery>();
            TranslateCommandMock = new Mock<ITranslateCommand>();

            TranslatorViewModel = new Presentation.ViewModels.TranslatorViewModel(GetLanguagesQueryMock.Object, TranslateCommandMock.Object);
        }
    }
}
