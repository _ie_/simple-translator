﻿using System;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.MainPageViewModel
{
    public class When_call_MainPageViewModel_ctor : MainPageViewModelBase
    {
        [Fact]
        public void then_it_returns_passed_in_ctor_translator()
        {
            // Arrange

            // Act
            var sut = new Presentation.ViewModels.MainPageViewModel(TranslatorViewModel);

            // Assert
            TranslatorViewModel.ShouldBeSameAs(sut.Translator);
        }

        [Fact]
        public void then_it_fails_if_translator_is_null()
        {
            // Arrange

            // Act
            void Action() => new Presentation.ViewModels.MainPageViewModel(null);

            // Assert
            Should.Throw<ArgumentNullException>((Action)Action);
        }
    }
}