﻿using System.Threading.Tasks;
using Application.UseCases.Commands;
using Application.UseCases.Queries;
using Moq;
using Presentation.Models;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class TranslatorViewModelBase
    {
        protected static LanguageModel LanguageAuto = new LanguageModel { Key = "AUTO", Name = "Auto" };
        protected static LanguageModel LanguageEn = new LanguageModel { Key = "EN", Name = "English" };
        protected static LanguageModel LanguageDe = new LanguageModel { Key = "DE", Name = "German" };
        protected static LanguageModel LanguageInvalid = new LanguageModel { Key = "INVALID", Name = "Invalid" };

        protected readonly Mock<IGetLanguagesQuery> GetLanguagesQueryMock;
        protected readonly Mock<ITranslateCommand> TranslateCommandMock;

        protected TranslatorViewModelBase()
        {
            GetLanguagesQueryMock = new Mock<IGetLanguagesQuery>();
            TranslateCommandMock = new Mock<ITranslateCommand>();

            GetLanguagesQueryMock.Setup(x => x.Execute()).Returns(Task.FromResult(new GetLanguagesResult
            {
                Languages = new [] { LanguageAuto.Key, LanguageEn.Key, LanguageDe.Key }
            }));

            TranslateCommandMock
                .Setup(x => x.Execute(It.IsAny<TranslationRequest>()))
                .Returns<TranslationRequest>(request =>
                {
                    var sourceLanguage = request.SourceLanguage;
                    var translations = new string[0];
                    if (request.Word == "hello" && (request.SourceLanguage == "EN" || request.SourceLanguage == "AUTO"))
                    {
                        sourceLanguage = "EN";
                        translations = new[] {"hallo", "moin"};
                    }

                    return Task.FromResult(new TranslationResponse
                    {
                        SourceLanguage = sourceLanguage,
                        TargetLanguage = request.TargetLanguage,
                        Word = request.Word,
                        Translations = translations
                    });
                });

        }

        protected Presentation.ViewModels.TranslatorViewModel CreateViewModel()
        {
            return new Presentation.ViewModels.TranslatorViewModel(GetLanguagesQueryMock.Object, TranslateCommandMock.Object);
        }
    }
}
