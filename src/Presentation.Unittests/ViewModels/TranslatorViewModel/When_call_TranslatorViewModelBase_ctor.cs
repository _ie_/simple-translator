﻿using System;
using System.Linq;
using Moq;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_call_TranslatorViewModelBase_ctor : TranslatorViewModelBase
    {
        [Fact]
        public void then_set_related_properties()
        {
            // Arrange

            // Act
            var sut = CreateViewModel();

            // Assert
            sut.SourceLanguage.ShouldNotBeNull();
            sut.TargetLanguage.ShouldNotBeNull();
            sut.SourceLanguages.ShouldNotBeEmpty();
            sut.TargetLanguages.ShouldNotBeEmpty();
            sut.SourceLanguage.Key.ShouldBe("AUTO");
            sut.TargetLanguage.Key.ShouldBe("EN");
            sut.SourceLanguages.Select(x => x.Key).ShouldBe(new[] {"AUTO", "EN", "DE"});
            sut.SourceLanguages.Select(x => x.Name).ShouldBe(new[] {"Auto", "English", "German"});
            sut.TargetLanguages.Select(x => x.Key).ShouldBe(new[] {"EN", "DE"});
            sut.TargetLanguages.Select(x => x.Name).ShouldBe(new[] {"English", "German"});
        }

        [Fact]
        public void then_request_list_of_languages()
        {
            // Arrange

            // Act
            var sut = CreateViewModel();

            // Assert
            GetLanguagesQueryMock.Verify(x => x.Execute(), Times.Once);
        }

        [Fact]
        public void then_it_fails_if_getLanguagesQuery_is_null()
        {
            // Arrange

            // Act
            void Action() => new Presentation.ViewModels.TranslatorViewModel(null, TranslateCommandMock.Object);

            // Assert
            Should.Throw<ArgumentNullException>((Action)Action);
        }

        [Fact]
        public void then_it_fails_if_translateCommand_is_null()
        {
            // Arrange

            // Act
            void Action() => new Presentation.ViewModels.TranslatorViewModel(GetLanguagesQueryMock.Object, null);

            // Assert
            Should.Throw<ArgumentNullException>((Action)Action);
        }

    }
}