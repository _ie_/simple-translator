﻿using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_check_CanExecute_of_SwapLanguages_command : TranslatorViewModelBase
    {
        [Fact]
        public void then_it_returns_false_if_SourceLanguage_is_Auto()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageAuto;

            // Act
            var result = sut.SwapLanguagesCommand.CanExecute();

            // Assert
            result.ShouldBe(false);
        }

        [Fact]
        public void then_it_returns_true_if_SourceLanguage_is_not_Auto()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;

            // Act
            var result = sut.SwapLanguagesCommand.CanExecute();

            // Assert
            result.ShouldBe(true);
        }
    }
}