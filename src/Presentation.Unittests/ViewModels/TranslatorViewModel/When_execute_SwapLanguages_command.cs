﻿using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_execute_SwapLanguages_command : TranslatorViewModelBase
    {
        [Fact]
        public void then_it_swaps_SourceLanguage_and_TargetLanguage()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;

            // Act
            sut.SwapLanguagesCommand.Execute();

            // Assert
            sut.TargetLanguage.ShouldBe(LanguageEn);
            sut.SourceLanguage.ShouldBe(LanguageDe);
        }
    }
}