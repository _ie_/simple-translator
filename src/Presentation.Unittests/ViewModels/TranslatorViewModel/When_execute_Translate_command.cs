﻿using System.Threading.Tasks;
using Application.UseCases.Commands;
using Moq;
using Presentation.Models;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_execute_Translate_command : TranslatorViewModelBase
    {
        [Fact]
        public void then_it_executes_TranslateCommand_once()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;
            sut.Word = "hello";

            // Act
            sut.TranslateCommand.Execute();

            // Assert
            TranslateCommandMock.Verify(x => x.Execute(It.IsAny<TranslationRequest>()), Times.Once);
        }

        [Fact]
        public void then_it_sets_returned_Translations()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;
            sut.Word = "hello";

            // Act
            sut.TranslateCommand.Execute();

            // Assert
            sut.Translations.ShouldBe(new[] {"hallo", "moin"});
        }

        [Fact]
        public void then_if_successfully_translated_it_sets_Status_to_Success()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;
            sut.Word = "hello";

            // Act
            sut.TranslateCommand.Execute();

            // Assert
            sut.Status.ShouldBe(TranslationStatus.Success);
        }

        [Fact]
        public void then_if_no_translations_found_it_sets_Status_to_Error()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;
            sut.Word = "hello111";

            // Act
            sut.TranslateCommand.Execute();

            // Assert
            sut.Status.ShouldBe(TranslationStatus.Error);
        }

        [Fact]
        public void then_it_sets_Status_to_Translating_before_TranslateCommand_call()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;
            sut.Word = "long_translation";

            var status = TranslationStatus.Pending;
            TranslateCommandMock
                .Setup(x => x.Execute(It.IsAny<TranslationRequest>()))
                .Callback(() => status = sut.Status)
                .Returns(Task.FromResult(new TranslationResponse
                {
                    SourceLanguage = LanguageEn.Key,
                    TargetLanguage = LanguageDe.Key,
                    Translations = new string[0],
                    Word = "long_translation"
                }));

            // Act
            sut.TranslateCommand.Execute();

            // Assert
            status.ShouldBe(TranslationStatus.Translating);
        }
    }
}