﻿using System;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_set_SourceLanguage_property : TranslatorViewModelBase
    {
        [Fact]
        public void then_TargetLanguage_should_not_be_same()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;

            // Act
            sut.SourceLanguage = sut.TargetLanguage;

            // Assert
            sut.SourceLanguage.Key.ShouldNotBe(sut.TargetLanguage.Key);
        }

        [Fact]
        public void then_it_should_not_accept_invalid_value()
        {
            // Arrange
            var sut = CreateViewModel();

            // Act
            void Action() => sut.SourceLanguage = LanguageInvalid;

            // Assert
            Should.Throw<InvalidOperationException>((Action)Action);
        }

        [Fact]
        public void then_it_should_accept_auto_value()
        {
            // Arrange
            var sut = CreateViewModel();

            // Act
            sut.SourceLanguage = LanguageAuto;

            // Assert
            sut.SourceLanguage.Key.ShouldBe(LanguageAuto.Key);
            sut.SourceLanguage.Name.ShouldBe(LanguageAuto.Name);
        }
    }
}