﻿using System;
using Shouldly;
using Xunit;

namespace Presentation.Unittests.ViewModels.TranslatorViewModel
{
    public class When_set_TargetLanguage_property : TranslatorViewModelBase
    {
        [Fact]
        public void then_SourceLanguage_should_not_be_same()
        {
            // Arrange
            var sut = CreateViewModel();
            sut.SourceLanguage = LanguageEn;
            sut.TargetLanguage = LanguageDe;

            // Act
            sut.TargetLanguage = sut.SourceLanguage;

            // Assert
            sut.SourceLanguage.Key.ShouldNotBe(sut.TargetLanguage.Key);
        }

        [Fact]
        public void then_it_should_not_accept_invalid_value()
        {
            // Arrange
            var sut = CreateViewModel();

            // Act
            void Action() => sut.TargetLanguage = LanguageInvalid;

            // Assert
            Should.Throw<InvalidOperationException>((Action)Action);
        }

        [Fact]
        public void then_it_should_not_accept_auto_value()
        {
            // Arrange
            var sut = CreateViewModel();

            // Act
            void Action() => sut.TargetLanguage = LanguageAuto;

            // Assert
            Should.Throw<InvalidOperationException>((Action) Action);
        }
    }
}