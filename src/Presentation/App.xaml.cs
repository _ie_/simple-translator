using Catel.IoC;
using Catel.Logging;
using Presentation.ViewModels;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace Presentation
{
	public partial class App
    {
        private static readonly ILog Log = LogManager.GetCurrentClassLogger();

        public App ()
		{
		    LogManager.AddDebugListener();

		    Log.Info("Initializing application");

		    var serviceLocator = ServiceLocator.Default;
		    Application.DependencyRegistrator.Register(serviceLocator);
		    Persistance.DependencyRegistrator.Register(serviceLocator);

            serviceLocator.RegisterType<TranslatorViewModel>();
            serviceLocator.RegisterType<MainPageViewModel>();

            InitializeComponent();

            MainPage = new MainPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
