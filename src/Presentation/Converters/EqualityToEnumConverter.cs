﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Presentation.Converters
{
    // TODO: extracted Impl class for tests... hack.
    public class EqualityToEnumConverterImpl
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Equals(value, parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
    public class EqualityToEnumConverter : EqualityToEnumConverterImpl, IValueConverter
    {
    }
}
