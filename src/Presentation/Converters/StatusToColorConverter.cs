﻿using System;
using System.Globalization;
using Presentation.Models;
using Xamarin.Forms;

namespace Presentation.Converters
{
    public class StatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TranslationStatus status)
            {
                var resourceDictionary = Xamarin.Forms.Application.Current.Resources;
                switch (status)
                {
                    case TranslationStatus.Pending:
                    case TranslationStatus.Translating:
                        return (Color) resourceDictionary["Header.BackgroundColor.Regular"];

                    case TranslationStatus.Error:
                        return (Color) resourceDictionary["Header.BackgroundColor.Error"];

                    case TranslationStatus.Success:
                        return (Color) resourceDictionary["Header.BackgroundColor.Success"];
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}