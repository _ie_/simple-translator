﻿using Catel.Data;

namespace Presentation.Models
{
    public class LanguageModel : ModelBase
    {
        public string Key { get; set; }

        #region Name

        public string Name
        {
            get => GetValue<string>(NameProperty);
            set => SetValue(NameProperty, value);
        }
        public static readonly PropertyData NameProperty = RegisterProperty(nameof(Name), typeof(string));

        #endregion
    }
}