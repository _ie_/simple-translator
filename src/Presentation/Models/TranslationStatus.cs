﻿namespace Presentation.Models
{
    public enum TranslationStatus
    {
        Pending,
        Translating,
        Success,
        Error
    }
}