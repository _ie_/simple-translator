﻿using System;
using Catel.Data;
using Catel.MVVM;

namespace Presentation.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel(TranslatorViewModel translator)
        {
            Translator = translator ?? throw new ArgumentNullException(nameof(translator));
        }

        #region TargetLanguage

        public TranslatorViewModel Translator
        {
            get => GetValue<TranslatorViewModel>(TranslatorProperty);
            set => SetValue(TranslatorProperty, value);
        }
        public static readonly PropertyData TranslatorProperty = RegisterProperty(nameof(Translator), typeof(TranslatorViewModel));

        #endregion
    }
}
