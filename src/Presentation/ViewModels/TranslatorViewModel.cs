﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Application.UseCases.Commands;
using Application.UseCases.Queries;
using Catel.Collections;
using Catel.Data;
using Catel.Logging;
using Catel.MVVM;
using Presentation.Models;

namespace Presentation.ViewModels
{
    public class TranslatorViewModel : ViewModelBase
    {
        private static readonly ILog Log = LogManager.GetCurrentClassLogger();
        private static readonly string AutoLanguageKey = "AUTO";
        private static readonly IDictionary<string, LanguageModel> KnownLanguages = new Dictionary<string, LanguageModel>
        {
            { AutoLanguageKey, new LanguageModel {Key = AutoLanguageKey, Name = "Auto"} },
            { "EN", new LanguageModel {Key = "EN", Name = "English"} },
            { "DE", new LanguageModel {Key = "DE", Name = "German"} },
            { "FR", new LanguageModel {Key = "FR", Name = "French"} },
            { "IT", new LanguageModel {Key = "IT", Name = "Italian"} },
            { "RU", new LanguageModel {Key = "RU", Name = "Russian"} },
        };

        private readonly List<LanguageModel> _languages = new List<LanguageModel>();
        private readonly IGetLanguagesQuery _getLanguagesQuery;
        private readonly ITranslateCommand _doTranslateCommand;

        public TranslatorViewModel(IGetLanguagesQuery getLanguagesQuery, ITranslateCommand translateCommand)
        {
            _getLanguagesQuery = getLanguagesQuery ?? throw new ArgumentNullException(nameof(getLanguagesQuery));
            _doTranslateCommand = translateCommand ?? throw new ArgumentNullException(nameof(translateCommand));

            FillLanguages();
        }

        public ObservableCollection<LanguageModel> SourceLanguages { get; } = new ObservableCollection<LanguageModel>();
        public ObservableCollection<LanguageModel> TargetLanguages { get; } = new ObservableCollection<LanguageModel>();
        public ObservableCollection<string> Translations { get; } = new ObservableCollection<string>();

        public LanguageModel SourceLanguage
        {
            get => GetValue<LanguageModel>(SourceLanguageProperty);
            set
            {
                if (SourceLanguages.All(x => x.Key != value?.Key))
                    throw new InvalidOperationException("Expected value from TargetLanguages collection.");

                SetValue(SourceLanguageProperty, value);

                if (TargetLanguage?.Key == SourceLanguage?.Key)
                {
                    TargetLanguage = TargetLanguages.FirstOrDefault(x => x.Key != SourceLanguage?.Key);
                }
            }
        }

        public static readonly PropertyData SourceLanguageProperty = RegisterProperty(nameof(SourceLanguage), typeof(LanguageModel));

        #region TargetLanguage

        public LanguageModel TargetLanguage
        {
            get => GetValue<LanguageModel>(TargetLanguageProperty);
            set
            {
                if (TargetLanguages.All(x => x.Key != value?.Key))
                    throw new InvalidOperationException("Expected value from TargetLanguages collection.");

                SetValue(TargetLanguageProperty, value);

                if (TargetLanguage?.Key == SourceLanguage?.Key)
                {
                    SourceLanguage = SourceLanguages.FirstOrDefault(x => x.Key != TargetLanguage?.Key);
                }
            }
        }
        public static readonly PropertyData TargetLanguageProperty = RegisterProperty(nameof(TargetLanguage), typeof(LanguageModel));

        #endregion

        #region Word

        public string Word
        {
            get => GetValue<string>(WordProperty);
            set => SetValue(WordProperty, value);
        }

        public static readonly PropertyData WordProperty = RegisterProperty(nameof(Word), typeof(string));

        #endregion

        #region TranslationStatus

        public TranslationStatus Status
        {
            get => GetValue<TranslationStatus>(StatusProperty);
            set => SetValue(StatusProperty, value);
        }
        public static readonly PropertyData StatusProperty = RegisterProperty(nameof(Status), typeof(TranslationStatus));

        #endregion

        #region SwapLanguagesCommand

        private Command _swapLanguagesCommand;

        public Command SwapLanguagesCommand => _swapLanguagesCommand ??
                                               (_swapLanguagesCommand = new Command(OnSwapLanguages, CanSwapLanguages));

        private void OnSwapLanguages()
        {
            var sourceLanguage = SourceLanguage;
            SourceLanguage = TargetLanguage;
            TargetLanguage = sourceLanguage;
        }

        private bool CanSwapLanguages() => SourceLanguage?.Key != AutoLanguageKey;

        #endregion

        #region TranslateCommand

        private Command _translateCommand;

        public Command TranslateCommand => _translateCommand ??
                                           (_translateCommand = new Command(OnTranslate, CanTranslate));

        private async void OnTranslate()
        {
            Status = TranslationStatus.Translating;

            var request = new TranslationRequest
            {
                SourceLanguage = SourceLanguage.Key,
                TargetLanguage = TargetLanguage.Key,
                Word = Word
            };
            var result = await _doTranslateCommand.Execute(request);

            if (result.Translations.Length == 0 || !KnownLanguages.ContainsKey(result.SourceLanguage))
            {
                Translations.Clear();
                Status = TranslationStatus.Error;
            }
            else
            {
                if (SourceLanguage.Key == AutoLanguageKey)
                {
                    // TODO: check why this does not work
                    SourceLanguage.Name = $"Auto ({KnownLanguages[result.SourceLanguage].Name})";
                }

                Translations.AddRange(result.Translations);
                Status = TranslationStatus.Success;
            }
        }

        private bool CanTranslate() => !string.IsNullOrWhiteSpace(Word) && SourceLanguage != null && TargetLanguage != null;

        #endregion

        protected override void OnPropertyChanged(AdvancedPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            switch (e.PropertyName)
            {
                case nameof(Word):
                case nameof(SourceLanguage):
                case nameof(TargetLanguage):
                    Status = TranslationStatus.Pending;
                    Translations.Clear();
                    break;
            }
        }

        private async void FillLanguages()
        {
            _languages.Clear();

            var result = await _getLanguagesQuery.Execute();

            foreach (var languageKey in result.Languages)
            {
                if (!KnownLanguages.ContainsKey(languageKey))
                {
                    Log.Warning("Unknown language: {0}", languageKey);
                    continue;
                }

                _languages.Add(KnownLanguages[languageKey]);
            }
            
            SourceLanguages.Clear();
            SourceLanguages.AddRange(_languages);

            SourceLanguage = SourceLanguages.FirstOrDefault(x => x.Key == AutoLanguageKey) ??
                             SourceLanguages.FirstOrDefault();
            
            TargetLanguages.Clear();
            TargetLanguages.AddRange(_languages.Where(x => x.Key != AutoLanguageKey));

            TargetLanguage = TargetLanguages.FirstOrDefault(x => x.Key != SourceLanguage?.Key);
        }
    }
}
