﻿using Xamarin.Forms.Xaml;

namespace Presentation.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TranslatorView
	{
		public TranslatorView ()
		{
			InitializeComponent ();
		}
	}
}