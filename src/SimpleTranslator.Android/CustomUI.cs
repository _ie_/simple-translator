﻿using Android.Content;
using Android.Views;
using CustomUI;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(BetterPickerRenderer))]

namespace CustomUI
{
    public class BetterPickerRenderer : PickerRenderer
    {
        public BetterPickerRenderer(Context context) : base(context)
        { }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Gravity = GravityFlags.Center;
            }
        }
    }
}